const app = Vue.createApp({
    data() {
        return {
            counter: 0,
        }
    },
    watch: {
        counter(value) {
            if (value === 37) {
                const newValue  = this;
                setTimeout(function() {
                    newValue.counter = 0
                },5000)
            }
        }
    },
    computed: {
        result() {
            if (this.counter < 37) {
                return 'Not there yet'
            } else if ( this.counter > 37) {
                return 'Too much!'
            } else {
                return 'Great!'
            }
        }
    },
    methods: {
        addNumber(num) {
         this.counter = this.counter + num
        },
        removeNumber(num) {
         this.counter = this.counter - num
        }
    }
})

app.mount('#assignment')